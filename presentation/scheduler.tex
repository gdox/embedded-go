\section{Scheduler}
\makecurrentsection

\begin{frame}
	\frametitle{\currentname}
	Three kinds of concurrency:

	\begin{itemize}
		\item Threads
		\item Fibers
		\item Actors (out of scope)
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Threads}
	\begin{itemize}
		\item Scheduled by the OS
		\item Preemptive
		\item Context switching expensive
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Fibers}
	\begin{itemize}
		\item Scheduled by the user
		\item Cooperative
		\item Context switching cheap
		\item Relies on a runtime
		\item Language support: Go, Erlang, Pony, Crystal ...
		\item Frameworks: libreactor, vectr, asyncio, tokio ...
		\item How you get on top of \texttt{https://www.techempower.com/benchmarks/}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Fibers}
	Many different implementations
	\begin{itemize}
		\item Stackful vs Stackless
		\item Global Queue vs Work Stealing vs Hybrid
		\item ...
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Fibers}
	Many different implementations
	\begin{itemize}
		\item Stackful vs Stackless
		\item Global Queue vs Work Stealing vs Hybrid
		\item ...
	\end{itemize}

	Golang: Stackful Hybrid Goroutines
\end{frame}

\begin{frame}[fragile]
	\frametitle{Fibers}
	Consider this function:

	\begin{lstlisting}
	func Retrieve() int {
		i := 3
		j := file.ReadInt()
		return i + j
	}
	\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Stackless Fibers}
	Basic implementation:

	\begin{lstlisting}
	interface Goroutine {
		advance() (interface{}, err)
	}
	\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Stackless Fibers}
	\begin{lstlisting}
	type RetrieveRoutine struct {
		i int,
		j int,
		state int
		filestate FileState
	}
	\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Stackless Fibers}
	\begin{lstlisting}
	func (r RetrieveRoutine) advance() (int, err) {
		switch r.state {
			case 0:
				r.i = 3
				r.filestate = file.ReadIntFuture()
				r.state = 1
				return (nil, Error)
			case 1:
				if r.filestate.Complete() {
					r.j = r.filestate.Get()
					r.state = 2
				}
				return (nil, Error)
			case 2:
				return (r.i + r.j, nil)
		}
	}
	\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Stackful Fibers}
	\begin{lstlisting}
	func advance() int {
		i := 3
		j_fut := file.ReadIntFuture()
		do {
			runtime.park(j_fut)
		} while not j_fut.Complete()
		j := j_fut.Get()
		return i + j
	}
	\end{lstlisting}
\end{frame}

\begin{frame}
	\frametitle{Stackful Fibers in Go}
	Synchronisation point: place where the coroutine suggests yielding to the scheduler
	\begin{itemize}
		\item Function calls
		\item Sending and receiving from a channel, synchronisation
		\item Launching a goroutine
		\item Loops (since 1.12)
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{\currentname}

	Main behaviour of scheduler:
	\begin{itemize}
		\item Pull coroutine from queue
		\item Advance coroutine
		\item If coroutine is not complete, push coroutine back on queue
		\item Rinse and Repeat
	\end{itemize}

	Multithreaded version: use a global queue and multiple schedulers.

	\pause
	Additional queues for io, timers ... (usually implemented through epoll)
\end{frame}

\begin{frame}
	\frametitle{Mutex}

	\textbf{Sidenote: Mutex}

	Every mutex holds a queue of contenders.

	Locking a mutex:
	\begin{itemize}
		\item If the mutex is unlocked, lock and schedule
		\item If the mutex is not locked, push on mutex queue
	\end{itemize}

	Unlocking a mutex:
	\begin{itemize}
		\item If the mutex queue is empty, unlock the mutex
		\item If the mutex queue is not empty, pull and schedule
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{\currentname}
	Deadlock: when all coroutines are in a mutex-like queue.
\end{frame}

\begin{frame}
	\frametitle{\currentname}
	\textbf{Global queue}

	Advantages:
	\begin{itemize}
		\item Low latency
		\item Simple implementation
	\end{itemize}
	Disadvantages:
	\begin{itemize}
		\item Low throughput
		\item Contention
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{\currentname}
	Alternative: Giving every thread its own queue.
	
	But: might starve a thread!

	Solution: \textbf{Work Stealing}
\end{frame}

\begin{frame}
	\frametitle{Work Stealing}
	Basic algorithm:
	
	\begin{itemize}
		\item Drain all coroutines in your queue
		\item If your queue is empty:
		\begin{itemize}
			\item Pick a random other scheduler
			\item Move half of his items to your queue
		\end{itemize}
		\item If your queue is still empty, park the thread
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{\currentname}
	\textbf{Work stealing}

	Advantages:
	\begin{itemize}
		\item High throughput
		\item Much less starvation
	\end{itemize}
	Disadvantages:
	\begin{itemize}
		\item Relatively high overhead
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{\currentname}

	\textbf{Golang Scheduling configuration}

	By default, Golang spawns NumCPU workerthreads.

	Can be configured using \texttt{GOMAXPROCS}

	\textit{This does not limit the number of threads!}
\end{frame}

\begin{frame}
	\frametitle{\currentname}

	\textbf{Golang Scheduling configuration}
	
	Sometimes goroutines need to be scheduled on a single thread.
	
	Example: OpenGL context.

	Solution in standard library: \texttt{runtime.LockOSThread()}
\end{frame}

\begin{frame}
	\frametitle{LockOSThread}
	Locks a goroutine to a single thread:

	\begin{itemize}
		\item No other thread will advance this goroutine.
		\item No other goroutine will be advance by the current thread.
	\end{itemize}

	\pause
	Pitfalls:
	\begin{itemize}
		\item Only initialize the context after using \texttt{LockOSThread}
		\item Do not share the context with other goroutines
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Summary}
	\textbf{Takeaway:} Golang has a fast scheduler, despite this being a runtime overhead.
\end{frame}
