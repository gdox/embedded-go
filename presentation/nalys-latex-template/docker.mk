WORK_DIR?=$(strip $(notdir $(CURDIR:%/=%)))/

include $(BASE_DIR)nalys-latex-template/vars.mk

DOCKER_RUN=
MK=make
ifndef IN_DOCKER
DOCKER_RUN=docker run --rm -ti --name nalys-latex-builder --user="$(shell id -u):$(shell id -g)" -v "$(BASE_DIR):/app" -w "/app/$(WORK_DIR)" $(NALYS_TEX_DOCKER_IMAGE)
MK=make IN_DOCKER=1
endif

ifdef IN_DOCKER
all: build-all
else
all:
	@echo "+++ checking docker image (may take a while the first time ...)"
	@make -C nalys-latex-template pull
	$(DOCKER_RUN) $(MK)
endif

