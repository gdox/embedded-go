NALYS_DOCKER_REGISTRY=registry.gitlab.com
NALYS_TEX_DOCKER_VERSION?=v1.4
NALYS_TEX_DOCKER_IMAGE?=$(NALYS_DOCKER_REGISTRY)/nalys-nit-courses-common/nalys-latex-template:$(NALYS_TEX_DOCKER_VERSION)
