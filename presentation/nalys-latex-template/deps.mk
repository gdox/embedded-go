ifndef OUTPUT
$(error OUTPUT is not set)
endif

BASE_DIR?=$(CURDIR)/../
PDFS_DIR=$(BASE_DIR)/build

MAIN_TEX?=main.tex

TEMPLATE_DIR:=$(BASE_DIR)/nalys-latex-template/

include $(TEMPLATE_DIR)/docker.mk

DEPS:=\
	beamercolorthemenalys.sty\
	beamerfontthemenalys.sty\
	beamerinnerthemenalys.sty\
	beamerouterthemenalys.sty\
	beamerthemenalys.sty\

DEPS:=$(addprefix $(TEMPLATE_DIR), $(DEPS))

build-all: code | pdf
.PHONY: pdf code package

ifdef CODE_DIR
code:
	$(DOCKER_RUN) $(MK) -C $(CODE_DIR)
endif

pdf:
	$(DOCKER_RUN) latexmk -jobname="$(OUTPUT)" -xelatex -pdf $(MAIN_TEX)

package:
	@mkdir -p $(PDFS_DIR) && cp -v $(CURDIR)/$(OUTPUT).pdf $(PDFS_DIR)

clean:
	@rm -rvf $(shell cat $(BASE_DIR)/.gitignore)
	@rm -rvf *.pdf
