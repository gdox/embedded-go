/*
 * hello_world.c
 * Copyright (C) 2020 gerwin
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main(int argc, char * argv[])
{

	printf("Hello world!");
    return 0;
}

