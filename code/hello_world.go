//
// hello_world.go
// Copyright (C) 2020 gerwin <gerwin@gerwin-linux>
//
// Distributed under terms of the MIT license.
//

package main

import(
	"fmt"
)

func main() {
	fmt.Println("Hello world")
}
