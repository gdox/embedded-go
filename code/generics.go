//
// generics.go
// Copyright (C) 2020 gerwin <gerwin@gerwin-linux>
//
// Distributed under terms of the MIT license.
//

package main

func Get(x []int) int {
	x[0] = 5
	return x[3]
}

func GetTwo(hashmap map[int]string) {
	hashmap[1] = "one"
	hashmap[0] = "zero"
}

type MyInterface interface {
	getData() int
}

type MyStruct struct {
	data int
}

func (s MyStruct) getData() int {
	return s.data
}

func  call(m MyInterface) int {
	return m.getData()
}

func Global() int {
	s := MyStruct {data : 5}
	return call(s)
}

func main() {
	Global()
}
